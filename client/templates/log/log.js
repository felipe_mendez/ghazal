Template.log.events({
    'submit #login': function(event) {
        event.preventDefault();
        Meteor.loginWithPassword(
            event.target.username.value,
            event.target.password.value,
            function (e) {
                if(e) {
                    console.log(e);
                } else {
                    Router.go('/')
                }
            }
        )
    }
})

