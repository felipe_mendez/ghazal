Template.createAccount.events({
    'submit #createUser': function(event) {
        event.preventDefault();
        var id = Accounts.createUser({
            username: event.target.username.value,
            email: event.target.email.value,
            password: event.target.password.value
        }, function(e) {
            if(e) {
                console.log(e);
            } else {
                Router.go('/')
            }
        })
    }
})
