Template.services.helpers({
    services: function() {
        Meteor.subscribe('db');
        return db.find({}).map(function(service) {
            if(service.options) {
                service.options = service.options.map(function (option, i) {
                    option.index = i;
                    option.id = service._id
                    return option
                })
            }
            return service
        });
    }
});

Template.services.events({
    'click #addService': function () {
        Meteor.call('addService')
    },
    'click .removeService': function(e) {
        Meteor.call('removeService', this)
    },
    'input .service': function(e) {
        this.value = e.target.value;
        Meteor.call('updateServiceName', this)
    },
    'click .addOption': function(e) {
        Meteor.call('addServiceOption', this._id)
    },
    'click .removeOption': function(e) {
        Meteor.call('removeOption', this)
    },
    'input .name': function(e) {
        this.value = e.target.value;
        Meteor.call('updateOptionName', this)
    },
    'input .price': function(e) {
        this.value = e.target.value;
        Meteor.call('updateOptionPrice', this)
    }
})

Template.services.onRendered(function () {
    Session.set('services', [{
        name: 'servicio',
        options: [
            {
                name: 'opcion 1',
                price: 300000
            },
            {
                name: 'opcion 2',
                price: 600000
            }
        ]
    }])
})