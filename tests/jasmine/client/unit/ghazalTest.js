describe('Services', () => {
    it('should exist', () => {
        expect(Services).toBeDefined();
    })

    it('should be a object', () => {
        expect(Services).toEqual(jasmine.any(Object))
    })

    it('should add a service', () => {
        expect(Services.addService).toBeDefined();
    })

    it('shoudnt have services', () => {
        expect(Services.getServices()).toEqual([]);
    })

    it('shoudnt add and return a services', () => {
        Services.addService('service1')
        expect(Services.getServices()).toEqual(['service1']);
    })
})

