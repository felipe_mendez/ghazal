var filters = {
    authenticate: function () {
        var user
        if (Meteor.loggingIn()) {
            console.log('[authenticate filter] loading')
            this.render('spinner')
        } else {
            user = Meteor.user()
            if (!user) {
                console.log('[authenticate filter] signin')
                this.layout('blank')
                this.render('log')
                return
            }
            // if (!emailVerified(user)) {
            //     console.log('[authenticate filter] awaiting-verification')
            //     this.layout('layout')
            //     this.render('awaiting-verification')
            //     return
            // }
            console.log('[authenticate filter] done');
            this.next()
        }
    },
    testFilter: function () {
        console.log('[test filter]')
        user = Meteor.user()
        if(Roles.userIsInRole(loggedInUser, ['admin','manage-users'])){
            this.render('services');
        } else {
            this.render('quote');
        }
        this.next()
    }
}

Router.onBeforeAction(function() {
    if (! Meteor.userId()) {
        this.render('log');
    } else {
        this.next();
    }
}, {
    except: ['/createAccount']
});

Router.route('/log', function () {
  'use strict';
  this.render('log');
  this.layout('blank');
});

Router.route('/', function () {
	'use strict';
   this.render('services');
   this.layout('layout')
    before: [filters.authenticate, filters.testFilter]
});

Router.route('/createAccount', function () {
  'use strict';
  this.render('createAccount');
  this.layout('layout');
});

Router.route('/quote', function () {
  'use strict';
  this.render('quote');
  this.layout('layout');
});