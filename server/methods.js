Meteor.methods({
    addService: function() {
        db.insert({name:'nuevo servicio'})
    },
    removeService: function(service) {
        db.remove(service._id)
    },
    updateServiceName: function(service) {
        var document = db.findOne(service._id);
        document.name = service.value
        db.update(service._id, document)
    },
    addServiceOption: function(id) {
        var document = db.findOne(id);
        if(!document.options) {
            document.options = [{name:'name', price: 123}]
        } else {
            document.options.push({name:'name', price: 123})
        }
        db.update(id, document)
    },
    removeOption: function(option) {
        var document = db.findOne(option.id);
        document.options.splice(option.index, 1)
        db.update(option.id, document)
    },
    updateOptionName: function(option) {
        var document = db.findOne(option.id);
        document.options[option.index].name = option.value;
        db.update(option.id, document)
    },
    updateOptionPrice: function(option) {
        var document = db.findOne(option.id);
        document.options[option.index].price = option.value;
        db.update(option.id, document)
    }
})
